package ajaymehta.applicationclassforobjects;

/**
 * Created by Avi Hacker on 7/17/2017.
 */

//

public class TvShow {

    // getter setter  we use to share data among all the activities and java files inside the project.

    private String showName;
    private Double showTime;

    public String getShowName() {
        return showName;
    }

    public void setShowName(String showName) {
        this.showName = showName;
    }



    public Double getShowTime() {
        return showTime;
    }

    public void setShowTime(Double showTime) {
        this.showTime = showTime;
    }
}
