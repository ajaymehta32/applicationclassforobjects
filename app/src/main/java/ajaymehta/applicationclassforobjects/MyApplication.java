package ajaymehta.applicationclassforobjects;

import android.app.Application;

/**
 * Created by Avi Hacker on 7/17/2017.
 */

//TODO...you will get this error if you not going to put MyApplication name in Manifest file
    // ClassCastException --> android.app.Application cannot be cast to ajaymehta.applicationclassforobjects.MyApplicatio

public class MyApplication extends Application {

    private TvShow tvShow = new TvShow();

    public TvShow getTvShowObject() {
        return tvShow;
    }

     // other way to do is .. is to create a object of TvShow in MainActivity .. then pass the object here .
    // the u can get the object anywhere in any java file or actity ..n use it ...
    // see the below example in project -->  POJOPitfallObject creation
 /*
    private TvShow tvShow;

    public TvShow getTvShow() {
        return tvShow;
    }

    public void setTvShow(TvShow tvShow) {
        this.tvShow = tvShow;
    }*/


}
// Why we using MyApplication class extends Application  to create our object  ....because ...
// its name suggest what we create in in Aplication class ..we can use that thing in whole application ,,,
//  so our object has  a concept (like a real person ) suppose humare pass do person hai  raju and shaam ...so hum raju ko ek apple dete hai ..to vo ab raju ke paas hai .jub humne khana hoga to raju se hi lena padega vapis ..agar hum shaam ko apple dene ke liye kahenge to kya vo de paega ?  nai ..kyuki uske pass to hai hi nai..
// same with object -->  what object Set (Hold) we can get from the same object... setter - getter
// to agar humne main activity main ek object banaya usko humne tv show ka name dia ...
// to hum Second activity main ..ek naya object (same class ka bana kar)  vo chiz get kar sate hai nai ,,,humne vahi same object chahea jisne use set kia tha

// to hum Application class main vo object bana sakte hai ..jo jaha bhi us object ki jarurat padega chahea get karna ho ..ya set karna ho...
// we can do with that single object...cool enough ....