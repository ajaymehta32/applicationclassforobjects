package ajaymehta.applicationclassforobjects;

/**
 * Created by Avi Hacker on 7/18/2017.
 */

public class JavaClass {


    // if you wanna use the same object here to set or get data ...(object we created in Application class)

    // yaha par humare pass context to nai hai to humn object to create kar nai kate

    public void createObject() {

   //     MyApplication appObject = (MyApplication) getApplicationContext(); //  hum application ka object bana nai sakte bina context ke
    //    TvShow   tvShowObject = appObject.getTvShowObject(); // to get bhi nai kar paayenge us class se object
        // so solution of single java files is that you have to pass object here
        // agar application ka karna hai to vo ..nai to Second Activity ya or koi activity main ya kahi bhi jaha movie object create kia hai
        // vaha se yaha send kar do..
        // see below for example
    }

    public void getAppObject(MyApplication obj) {// <-- like this


    }

    public void getTvShowObject(TvShow object) { // <-- like this..


    }
}
