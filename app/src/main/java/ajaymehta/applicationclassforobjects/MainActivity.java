package ajaymehta.applicationclassforobjects;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {

    TextView showName;

    MyApplication appObject;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        showName = (TextView) findViewById(R.id.tv_show_name);

        String name = "Prison";

        // gettting object of MyApplication
        appObject = (MyApplication) getApplication();   // or getApplication();  will also work ..u can replace getApplicationContext();

        appObject.getTvShowObject().setShowName(name);  // getting tvShow Object from MyApplication object  n setting our ...

   //     appObject.getTvShowObject().setShowTime(2.36); // 2 hours 36 min..   // also setting time of our movie..
    }

    public void enterName(View view) {

        showName.setText(appObject.getTvShowObject().getShowName());  // getting object of Tvshow class with help of myApplication object ..n then getting Name of movie n setting it to our textView..

        appObject.getTvShowObject().setShowTime(54.5); // setting time ..it setting when button click ..or u can set in onCreate method..

    }

    public void nextActivity(View view) {

        startActivity(new Intent(this, SecondActivity.class));
    }
}
