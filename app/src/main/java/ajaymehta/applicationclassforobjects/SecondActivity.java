package ajaymehta.applicationclassforobjects;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity {

    //TODO ..Warning  Warning  Warning ...
    // getting something (like object, Context) outside methods (here we are outside methods means we are not in any method )
    // will throw null Pointer exception...

/*   //  you can do this  getting  application Object (context) here ..fails  |   getting TvShow object here fails too ..suck..
    MyApplication appObject = (MyApplication) getApplicationContext(); // or getApplication()
    TvShow   tvShowObject = appObject.getTvShowObject();
*/




    MyApplication appObject; // so we took them globally  but getting  their object is done in onCreate

    TvShow   tvShowObject;   // getting object in onCreate

    TextView showNameTime;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        showNameTime = (TextView) findViewById(R.id.tv_show_time);

        appObject =  (MyApplication) getApplication();

        tvShowObject = appObject.getTvShowObject();

    }

    public void enterTime(View view) {

        showNameTime.setText(tvShowObject.getShowName()+"-"+tvShowObject.getShowTime());
    }
}
